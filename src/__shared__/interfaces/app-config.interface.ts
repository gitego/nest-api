export interface IAppConfig {
  port?: number;
  database: IDatabaseConfig;
  env?: any;
  jwt?: JwtConfig;
  allowedOrigins?: string[];
  swaggerEnabled?: boolean;
}

interface JwtConfig {
  secret: string;
  expiresIn: string | number;
}

interface IDatabaseConfig {
  username: string;
  database: string;
  password: string;
  host: string;
  port: number;
}
