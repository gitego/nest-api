import { Column, Entity, OneToOne, TableInheritance } from "typeorm";
import { AbstractEntity } from "../../__shared__/entities/abstract.entity";
import { ERole } from "../enums/role.enum";
import { Auth } from "./auth.entity";

@Entity("users")
@TableInheritance({ column: { type: "varchar", name: "type" } })
export class User extends AbstractEntity {
  @Column()
  type: string;
  @Column()
  role: ERole;
  @OneToOne(() => Auth, (auth) => auth.user)
  auth: Auth;
}
