import { Column, Entity, OneToOne } from "typeorm";
import { AbstractEntity } from "./abstract.entity";
import { User } from "./user.entity";

@Entity("auths")
export class Auth extends AbstractEntity {
  @Column()
  username: string;
  @Column()
  password: string;
  @Column({ nullable: true })
  refreshToken: string;
  @OneToOne(() => User, (user) => user.auth)
  user: User;
}
