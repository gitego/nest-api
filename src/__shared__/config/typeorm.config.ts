import "dotenv/config";
import { join } from "path";
import { DataSource, DataSourceOptions } from "typeorm";
import { appConfig } from "./app.config";

const config = appConfig();
export const typeormOptions = {
  type: "postgres",
  ...config.database,
  synchronize: false,
  dropSchema: false,
  logging: false,
  entities: [join(__dirname, "../../**/*.entity.{ts,js}")],
  migrationsTableName: "sql_migrations",
  migrations: ["dist/src/__migrations__/*{.ts,.js}"],
  migrationsRun: true,
};
export const AppDataSource = new DataSource(
  typeormOptions as DataSourceOptions,
);
