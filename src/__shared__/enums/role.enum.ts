export enum ERole {
  ADMIN = "ADMIN",
  STAFF = "STAFF",
  EMPLOYEE = "EMPLOYEE",
}
