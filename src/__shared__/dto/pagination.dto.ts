import { IsOptional } from "class-validator";
import { IPage } from "../interfaces/pagination.interface";

export class PaginationDto {
  @IsOptional()
  page?: string = "0";
  @IsOptional()
  size?: string = "10";
}

export class PageResponseDto<T> implements IPage<T> {
  items: T[];
  totalItems?: number;
  itemCount?: number;
  itemsPerPage?: number;
  totalPages?: number;
  currentPage?: number;
}
