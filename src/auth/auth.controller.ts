import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Post,
} from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import {
  BadRequestResponse,
  ErrorResponses,
  OkResponse,
  UnauthorizedResponse,
} from "../__shared__/decorators";
import { GenericResponse } from "../__shared__/dto/generic-response.dto";
import { User } from "../__shared__/entities/user.entity";
import { AuthService } from "./auth.service";
import { Authorize } from "./decorators/authorize.decorator";
import { GetUser } from "./decorators/get-user.decorator";
import { AdminLoginDto } from "./dto/login.dto";

@ApiTags("Authentication")
@Controller("auth")
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post("/login/admin")
  @OkResponse()
  @ErrorResponses(UnauthorizedResponse, BadRequestResponse)
  @HttpCode(HttpStatus.OK)
  async adminLogin(
    @Body() dto: AdminLoginDto,
  ): Promise<GenericResponse<{ refreshToken: string }>> {
    const { accessToken, refreshToken } = await this.authService.adminLogin(
      dto,
    );
    return new GenericResponse("Logged in successfully", {
      accessToken,
      refreshToken,
    });
  }

  @Get("/profile")
  @OkResponse()
  @Authorize()
  async getProfile(
    @GetUser() user: User,
  ): Promise<GenericResponse<Partial<User>>> {
    const payload = await this.authService.getProfile(user.id);
    return new GenericResponse("Profile retrieved successfully", payload);
  }
}
