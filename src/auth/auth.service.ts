import { Injectable } from "@nestjs/common";
import { User } from "../__shared__/entities/user.entity";
import { AdminLoginDto } from "./dto/login.dto";

@Injectable()
export class AuthService {
  constructor() {
    //
  }
  /**
   * Login the admin
   * @param dto login dto
   * @returns tokens
   */
  async adminLogin(dto: AdminLoginDto) {
    return { accessToken: "test", refreshToken: "test" };
  }
  /**
   * Get profile
   * @param id user id
   * @returns profile
   */
  async getProfile(id: number) {
    return new User();
  }
}
