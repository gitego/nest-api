import { Injectable, UnauthorizedException } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { PassportStrategy } from "@nestjs/passport";
import { InjectRepository } from "@nestjs/typeorm";
import "dotenv/config";
import { ExtractJwt, Strategy } from "passport-jwt";
import { Repository } from "typeorm";
import { Auth } from "../../__shared__/entities/auth.entity";
import { User } from "../../__shared__/entities/user.entity";
import { IJwtPayload } from "../interfaces/jwt.payload.interface";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    @InjectRepository(Auth)
    private authRepository: Repository<Auth>,
    private configService: ConfigService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: configService.get("jwt").secret,
    });
  }

  /**
   * Validate jwt user
   * @param payload Jwt payload
   * @returns user
   */
  async validate(payload: IJwtPayload): Promise<User> {
    const { id } = payload;
    const auth = await this.authRepository.findOne({ where: { id } });
    if (!auth) {
      throw new UnauthorizedException();
    }
    return auth.user;
  }
}
