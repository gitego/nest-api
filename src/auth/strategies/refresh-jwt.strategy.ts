import { Injectable, UnauthorizedException } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { PassportStrategy } from "@nestjs/passport";
import { InjectRepository } from "@nestjs/typeorm";
import "dotenv/config";
import { Request } from "express";
import { ExtractJwt, Strategy } from "passport-jwt";
import { Repository } from "typeorm";
import { Auth } from "../../__shared__/entities/auth.entity";
import { User } from "../../__shared__/entities/user.entity";
import { IJwtPayload } from "../interfaces/jwt.payload.interface";

@Injectable()
export class JwtRefreshStrategy extends PassportStrategy(
  Strategy,
  "jwt-refresh",
) {
  constructor(
    @InjectRepository(Auth)
    private authRepository: Repository<Auth>,
    private configService: ConfigService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromExtractors([
        (request: Request) => request?.headers?.nest_refresh_jwt,
      ]),
      secretOrKey: configService.get("jwt").secret,
      passReqToCallback: true,
      ignoreExpiration: true,
    });
  }
  /**
   * Validate refresh token
   * @param req Request
   * @param payload Jwt payload
   * @returns user
   */
  async validate(req: Request, payload: IJwtPayload): Promise<User> {
    const { id } = payload;
    const auth = await this.authRepository.findOne({
      where: { id, refreshToken: req.cookies["nest_refresh_jwt"] },
    });
    if (!auth) throw new UnauthorizedException();
    return auth.user;
  }
}
