import { ERole } from "../../__shared__/enums/role.enum";

export interface IJwtPayload {
  id: number;
  role: ERole;
}
