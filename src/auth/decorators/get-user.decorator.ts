import { createParamDecorator, ExecutionContext } from "@nestjs/common";
import { User } from "../../__shared__/entities/user.entity";

/**
 * Get logged in user
 */
export const GetUser = createParamDecorator(
  (_data, ctx: ExecutionContext): User => {
    const req = ctx.switchToHttp().getRequest();
    return req.user;
  },
);
