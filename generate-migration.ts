import { execSync } from "child_process";

// Get the migration name as a command line argument
const migrationName = process.argv[2];

if (!migrationName) {
  console.error("Usage: node generate-migration.ts <MigrationName>");
  process.exit(1);
}

try {
  const command = `pnpm typeorm migration:generate src/__migrations__/${migrationName}`;
  execSync(command, { stdio: "inherit" });
} catch (error) {
  console.error("Error running the command:", error);
  process.exit(1);
}
