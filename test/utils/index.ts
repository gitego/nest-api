import { ValidationPipe } from "@nestjs/common";
import { Test, TestingModule } from "@nestjs/testing";
import { PostgreSqlContainer } from "@testcontainers/postgresql";
import * as cookieParser from "cookie-parser";
import { typeormOptions } from "../../src/__shared__/config/typeorm.config";
import { AppController } from "../../src/app.controller";
import { AppModule } from "../../src/app.module";
import { AppService } from "../../src/app.service";

export const initializeTestApp = async () => {
  const postgresqlContainer = await new PostgreSqlContainer(
    "postgres:14-alpine",
  )
    .withDatabase(typeormOptions.database)
    .withUsername(typeormOptions.username)
    .withPassword(typeormOptions.password)
    .withExposedPorts(5432)
    .start();
  typeormOptions.dropSchema = true;
  typeormOptions.synchronize = true;
  typeormOptions.migrationsRun = false;
  typeormOptions.port = postgresqlContainer.getMappedPort(5432);
  const moduleFixture: TestingModule = await Test.createTestingModule({
    imports: [AppModule],
    providers: [AppService],
    controllers: [AppController],
  }).compile();
  const app = moduleFixture.createNestApplication();
  app.use(cookieParser());
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
    }),
  );
  return app;
};
