import { INestApplication } from "@nestjs/common";
import * as request from "supertest";
import { initializeTestApp } from "./utils";

describe("AppController (e2e)", () => {
  let app: INestApplication;

  beforeAll(async () => {
    app = await initializeTestApp();
    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  it("Should get welcome message (GET)", async () => {
    const res = await request(app.getHttpServer()).get("/");
    expect(res.status).toEqual(200);
  });
});
