image: docker:git
services:
  - docker:dind

stages:
  - check-dev # This stage will consist of 3 different jobs (Build,Lint and Test)
  - docker_image_build # This stage consist of a job to build the docker image.
  - deploy # This stage consist of a job to deploy the built docker image.

variables:
  BASE_TAG_NAME: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG
  NODE_IMAGE_VERSION: node:18-alpine
  BASE_DEV_SERVER_PATH: /home/awesomedev/projects/nest/api
  BASE_STG_SERVER_PATH: /home/awesomedev/projects/nest/api
  BASE_PROD_SERVER_PATH: /home/awesomedev/projects/nest/api
  DEV_PROJECT_NAME: nest-api-dev
  STG_PROJECT_NAME: nest-api-stg
  PROD_PROJECT_NAME: nest-api-prod

.build_dockerfile_template: &build_dockerfile_template
  stage: docker_image_build
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build -f Dockerfile --cache-from $BASE_TAG_NAME:latest -t $BASE_TAG_NAME:$CI_COMMIT_SHORT_SHA -t $BASE_TAG_NAME:latest .
    - docker push $BASE_TAG_NAME:$CI_COMMIT_SHORT_SHA
    - docker push $BASE_TAG_NAME:latest

.deploy_template: &deploy_template
  script:
    - "which ssh-agent || (  apk update  && apk add openssh-client )"
    - "which rsync || ( apk update  && apk add rsync  )"
    - eval $(ssh-agent -s)
    # Inject the remote's private key
    - echo "$PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    # Append keyscan output into known hosts
    - ssh-keyscan $PUBLIC_IP_ADDRESS >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
    - echo "Deploying to $DEPLOY_ENVIRONMENT"
    - echo $CI_PROJECT_DIR
    - rsync --rsync-path=/usr/bin/rsync --delete -avuz --exclude=".*" /$CI_PROJECT_DIR/docker-compose/$COMPOSE_FILE_NAME $USER@$PUBLIC_IP_ADDRESS:$BASE_SERVER_PATH
    - rsync --rsync-path=/usr/bin/rsync --delete -avuz --exclude=".*" $ENV_FILE $USER@$PUBLIC_IP_ADDRESS:$BASE_SERVER_PATH/.env
    - echo "STARTING DOCKER IMAGE"
    - ssh $USER@$PUBLIC_IP_ADDRESS "docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY &&
      cd $BASE_SERVER_PATH &&
      docker image rm -f $BASE_TAG_NAME:latest &&
      docker pull $BASE_TAG_NAME:latest &&
      docker compose -f $BASE_SERVER_PATH/$COMPOSE_FILE_NAME -p  $PROJECT_NAME down -v &&
      docker compose -f $BASE_SERVER_PATH/$COMPOSE_FILE_NAME -p  $PROJECT_NAME up -d"

#This job help us build/create an artifact
Build_the_project:
  stage: check-dev
  image: $NODE_IMAGE_VERSION
  before_script:
    - corepack enable
    - corepack prepare pnpm@latest-8 --activate
    - pnpm config set store-dir .pnpm-store
  script:
    - pnpm install
    - pnpm build
  artifacts:
    paths:
      - node_modules/
  rules:
    - if: $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == 'dev'
  cache:
    key:
      files:
        - pnpm-lock.yaml
    paths:
      - .pnpm-store

#This job ensure our syntax is okay
eslint:
  stage: check-dev
  before_script:
    - corepack enable
    - corepack prepare pnpm@latest-8 --activate
    - pnpm config set store-dir .pnpm-store
  script:
    - pnpm lint
  image: $NODE_IMAGE_VERSION
  rules:
    - if: $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == 'dev'
  needs:
    - Build_the_project

#This job will run all the available unit test in the project
run_test:
  stage: check-dev
  coverage: /All files[^|]*\|[^|]*\s+([\d\.]+)/
  artifacts:
    when: always
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage/cobertura-coverage.xml
  image: $NODE_IMAGE_VERSION
  before_script:
    - corepack enable
    - corepack prepare pnpm@latest-8 --activate
    - pnpm config set store-dir .pnpm-store
    - echo "*********** Creating .env file... ************"
    - cp $test_env .env
  after_script:
    - echo "*********** Deleting .env file after tests... ************"
    - rm .env
  script:
    - pnpm install
    # - pnpm test:e2e --runInBand
    - echo "*********** Run tests here. To be fixed later... ************"
  rules:
    - if: $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == 'dev'
    - if: $CI_COMMIT_BRANCH == 'dev'

#This  job will build the dockerfile to provide a docker image to be pushed inside gitlab registry
build_docker_file:
  stage: docker_image_build
  extends: .build_dockerfile_template
  only:
    - dev
    - staging
    - main

#This job will deploy our application to the development environment and run in a docker container
deploy_to_dev_environment:
  stage: deploy
  extends: .deploy_template
  variables:
    PRIVATE_KEY: $DEV_PRIVATE_KEY
    PUBLIC_IP_ADDRESS: $DEV_PUBLIC_IP_ADDRESS
    USER: $DEV_USER
    BASE_SERVER_PATH: $BASE_DEV_SERVER_PATH
    COMPOSE_FILE_NAME: dev.docker-compose.yaml
    ENV_FILE: $dev_env
    DEPLOY_ENVIRONMENT: development
    PROJECT_NAME: $DEV_PROJECT_NAME
  environment:
    name: development
  only:
    - dev

#This job will deploy our application to the development environment and run in a docker container
deploy_to_staging_environment:
  stage: deploy
  extends: .deploy_template
  variables:
    PRIVATE_KEY: $STG_PRIVATE_KEY
    PUBLIC_IP_ADDRESS: $STG_PUBLIC_IP_ADDRESS
    USER: $STG_USER
    BASE_SERVER_PATH: $BASE_STG_SERVER_PATH
    COMPOSE_FILE_NAME: stg.docker-compose.yaml
    ENV_FILE: $stg_env
    DEPLOY_ENVIRONMENT: staging
    PROJECT_NAME: $STG_PROJECT_NAME
  environment:
    name: staging
  only:
    - staging

#This job will deploy our application to the development environment and run in a docker container
deploy_to_prod_environment:
  stage: deploy
  extends: .deploy_template
  variables:
    PRIVATE_KEY: $PROD_PRIVATE_KEY
    PUBLIC_IP_ADDRESS: $PROD_PUBLIC_IP_ADDRESS
    USER: $PROD_USER
    BASE_SERVER_PATH: $BASE_PROD_SERVER_PATH
    COMPOSE_FILE_NAME: prod.docker-compose.yaml
    ENV_FILE: $prod_env
    DEPLOY_ENVIRONMENT: production
    PROJECT_NAME: $PROD_PROJECT_NAME
  environment:
    name: production
  only:
    - main
